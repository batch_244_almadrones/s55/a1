// import coursesData from '../data/coursesData';
import { useEffect, useState } from "react";
import CourseCard from "../components/CourseCard";

export default function Courses() {
  // State that will be used to store the courses retrieved from the database
  const [courses, setCourses] = useState([]);

  // Retrieves the courses from the database upon intial render of the "Courses" component

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        // Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components
        setCourses(
          data.map((course) => {
            return <CourseCard key={course._id} courseProp={course} />;
          })
        );
      });
  }, []);

  // The "map" method loops through the individual course objects in our array and returns a CourseCard component for each course
  // Multiple components created through the map method must have a unique key that will help React JS identify which component/elements have been changed, added, removed
  // Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our Courses using the courseProp

  // const courses = coursesData.map(course => {
  // 	return	(
  // 		<CourseCard key={course.id} courseProp = {course}/>
  // 	)
  // })

  return (
    <>
      <h1>Courses</h1>
      {courses}
    </>
  );
}
