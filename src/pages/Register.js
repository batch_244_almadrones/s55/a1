import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register() {
  const { user } = useContext(UserContext);

  // State hooks to store the values of the input fields
  const [email, setEmail] = useState("");
  const [firstName, setFN] = useState("");
  const [lastName, setLN] = useState("");
  const [mobileNo, setMobileNo] = useState("09");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  // Check if values are successfullt binded
  console.log(email);
  console.log(password1);
  console.log(password2);

  // Function to simulate user registration
  function registerUser(e) {
    // Prevents the page reloading
    e.preventDefault();
    register(email);
    // Clear input fields

    // alert("Thank you for registering!");
  }

  // Validation to enable submit button when all input fields are populated and both passwords match
  useEffect(() => {
    if (
      email !== "" &&
      (password1 !== "") & (password2 !== "") &&
      password1 === password2 &&
      firstName !== "" &&
      lastName !== "" &&
      mobileNo.length === 11
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password1, password2, firstName, lastName, mobileNo]);

  const register = (email) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Swal.fire({
            title: "Already registered",
            icon: "info",
            text: "Use another email address to register.",
          });
        } else if (data === false) {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password1,
              mobileNo: mobileNo,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              if (data == true) {
                Swal.fire({
                  title: "Successfully Registered",
                  icon: "success",
                  text: "Welcome to Zuitt.",
                });
                setEmail("");
                setPassword1("");
                setPassword2("");
                setFN("");
                setLN("");
                setMobileNo("09");
              } else {
                Swal.fire({
                  title: "Something went wrong.",
                  icon: "error",
                  text: "Please try again.",
                });
              }
            });
        }
      });
  };

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    // Invokes the registerUser function upon clicking on submit button
    <Form onSubmit={(e) => registerUser(e)} className="col-lg-4 col-xs-12">
      <Form.Group controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter First Name"
          required
          value={firstName}
          onChange={(e) => setFN(e.target.value)}
        />
      </Form.Group>{" "}
      <br />
      <Form.Group controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Last Name"
          required
          value={lastName}
          onChange={(e) => setLN(e.target.value)}
        />
      </Form.Group>
      <br />
      <Form.Group controlId="lastName">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Mobile Number"
          required
          value={mobileNo}
          onChange={(e) => setMobileNo(e.target.value)}
        />
      </Form.Group>
      <br />
      <Form.Group controlId="userEmail">
        <Form.Label>Email Address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>
      <br />
      <Form.Group controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          required
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
        />
      </Form.Group>
      <br />
      <Form.Group controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify password"
          required
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
        />
      </Form.Group>
      <br />
      {/*conditional render submit button based on isActive state*/}
      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
